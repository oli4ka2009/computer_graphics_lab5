using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class ButtonController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private bool aPressed = false;
    private bool dPressed = false;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            aPressed = true;
        }

        if (Input.GetKey(KeyCode.A) && aPressed)
        {
            MoveTriangleDown();
        }

        if (Input.GetKeyUp(KeyCode.A))
        {
            aPressed = false;
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            dPressed = true;
        }

        if (Input.GetKey(KeyCode.D) && dPressed)
        {
            MoveTriangleUp();
        }

        if (Input.GetKeyUp(KeyCode.D))
        {
            dPressed = false;
        }

        if (Input.GetKeyDown(KeyCode.Equals))
        {
            Camera mainCamera = Camera.main;

            GameObject[] arrowY = GameObject.FindGameObjectsWithTag("arrowY");
            foreach (GameObject gameObject1 in arrowY)
            {
                gameObject1.transform.position = new Vector2(gameObject1.transform.position.x, gameObject1.transform.position.y - 1);
            }
            GameObject[] arrowX = GameObject.FindGameObjectsWithTag("arrowX");
            foreach (GameObject gameObject1 in arrowX)
            {
                gameObject1.transform.position = new Vector2(gameObject1.transform.position.x - (float)1.75, gameObject1.transform.position.y);
            }

            mainCamera.orthographicSize -= 1;
        }

        if (Input.GetKeyDown(KeyCode.Minus))
        {
            Camera mainCamera = Camera.main;

            GameObject[] xGameObjects = GameObject.FindGameObjectsWithTag("coordinateLineX");
            GameObject[] yGameObjects = GameObject.FindGameObjectsWithTag("coordinateLineY");

            float largestX = 0;
            float largestY = 0;
            float smallestX = 0;
            float smallestY = 0;

            foreach (GameObject gameObject in xGameObjects)
            {
                if (gameObject.transform.position.x > largestX)
                    largestX = gameObject.transform.position.x;
                if (gameObject.transform.position.x < smallestX)
                    smallestX = gameObject.transform.position.x;
            }

            foreach (GameObject gameObject in yGameObjects)
            {
                if (gameObject.transform.position.y > largestY)
                    largestY = gameObject.transform.position.y;
                if (gameObject.transform.position.y < smallestY)
                    smallestY = gameObject.transform.position.y;
            }

            GameObject newLineX;
            GameObject newLineY;
            while (largestX < Screen.width / 2)
            {
                newLineX = GameObject.Instantiate(smallXLinePrefab);

                newLineX.transform.position = new Vector2(largestX + 1, smallXLinePrefab.transform.position.y);
                largestX++;
            }
            while (smallestX > -Screen.width)
            {
                newLineX = GameObject.Instantiate(smallXLinePrefab);

                newLineX.transform.position = new Vector2(smallestX - 1, smallXLinePrefab.transform.position.y);
                smallestX--;
            }
            while (largestY < Screen.height / 2)
            {
                newLineY = GameObject.Instantiate(smallYLinePrefab);

                newLineY.transform.position = new Vector2(smallYLinePrefab.transform.position.x, largestY + 1);
                largestY++;
            }
            while (smallestY > -Screen.height)
            {
                newLineY = GameObject.Instantiate(smallYLinePrefab);

                newLineY.transform.position = new Vector2(smallYLinePrefab.transform.position.x, smallestY - 1);
                smallestY--;
            }

            GameObject[] arrowY = GameObject.FindGameObjectsWithTag("arrowY");
            foreach (GameObject gameObject1 in arrowY)
            {
                gameObject1.transform.position = new Vector2(gameObject1.transform.position.x, gameObject1.transform.position.y + 1);
            }
            GameObject[] arrowX = GameObject.FindGameObjectsWithTag("arrowX");
            Debug.Log(arrowX.Length);
            foreach (GameObject gameObject1 in arrowX)
            {
                gameObject1.transform.position = new Vector2(gameObject1.transform.position.x + (float)1.75, gameObject1.transform.position.y);
            }

            mainCamera.orthographicSize += 1;
        }
    }

    public GameObject smallXLinePrefab;
    public GameObject smallYLinePrefab;

    public GameObject Point;
    private GameObject[] points;
    private LineRenderer[] lineRenderers;
    public float speed = 5f;

    public InputField Point1X;
    public InputField Point1Y;
    public InputField Point2X;
    public InputField Point2Y;
    public InputField Point3X;
    public InputField Point3Y;
    public InputField ScaleFactor;

    private Matrix4x4 translationMatrix1;
    private Matrix4x4 scaleMatrix1;
    byte[] bytes;

    public void GenerateTriangle()
    {
        int[] xCoords = new int[3];
        int[] yCoords = new int[3];

        if (!(int.TryParse(Point1X.text, out xCoords[0]) && int.TryParse(Point2X.text, out xCoords[1]) && int.TryParse(Point3X.text, out xCoords[2])
            && int.TryParse(Point1Y.text, out yCoords[0]) && int.TryParse(Point2Y.text, out yCoords[1]) && int.TryParse(Point3Y.text, out yCoords[2])))
        {
            Debug.Log("Error: Incorrectly entered coordinates");
            return;
        }

        points = new GameObject[3];
        lineRenderers = new LineRenderer[3];

        points[0] = GameObject.Instantiate(Point);
        points[1] = GameObject.Instantiate(Point);
        points[2] = GameObject.Instantiate(Point);

        for (int i = 0; i < 3; ++i)
        {
            points[i].transform.position = new Vector2(xCoords[i], yCoords[i]);
        }

        lineRenderers[0] = CreateLine(points[0], points[1]);
        lineRenderers[1] = CreateLine(points[1], points[2]);
        lineRenderers[2] = CreateLine(points[2], points[0]);

        RenderTexture renderTexture = new RenderTexture(imageWidth, imageHeight, 24);

        Camera.main.targetTexture = renderTexture;

        Texture2D texture = new Texture2D(imageWidth, imageHeight, TextureFormat.RGB24, false);

        Camera.main.Render();

        RenderTexture.active = renderTexture;
        texture.ReadPixels(new Rect(0, 0, imageWidth, imageHeight), 0, 0);
        texture.Apply();

        bytes = texture.EncodeToPNG();

        RenderTexture.active = null;
        Camera.main.targetTexture = null;
        Destroy(renderTexture);
        Destroy(texture);
    }
    private LineRenderer CreateLine(GameObject point1, GameObject point2)
    {
        GameObject obj = new GameObject();
        obj.tag = "Line";
        LineRenderer lineRenderer = obj.AddComponent<LineRenderer>();

        lineRenderer.material = new Material(Shader.Find("Sprites/Default"));

        lineRenderer.positionCount = 2;
        lineRenderer.startWidth = 0.1f;
        lineRenderer.endWidth = 0.1f;
        lineRenderer.startColor = Color.black;
        lineRenderer.endColor = Color.black;

        lineRenderer.SetPosition(0, point1.transform.position);
        lineRenderer.SetPosition(1, point2.transform.position);

        return lineRenderer;
    }
    private void MoveTriangleUp()
    {
        GameObject[] lineObjects = GameObject.FindGameObjectsWithTag("Line");
        for (int i = 0; i < lineObjects.Length; ++i)
        {
            GameObject.Destroy(lineObjects[i]);
        }

        Vector3 direction = new Vector3(1, 1, 0).normalized;

        Matrix4x4 translationMatrix = Matrix4x4.Translate(direction * speed * Time.deltaTime);

        translationMatrix1 = translationMatrix;

        foreach (GameObject point in points)
        {
            point.transform.position = translationMatrix.MultiplyPoint(point.transform.position);
        }

        lineRenderers[0] = CreateLine(points[0], points[1]);
        lineRenderers[1] = CreateLine(points[1], points[2]);
        lineRenderers[2] = CreateLine(points[2], points[0]);
    }
    private void MoveTriangleDown()
    {
        GameObject[] lineObjects = GameObject.FindGameObjectsWithTag("Line");
        for (int i = 0; i < lineObjects.Length; ++i)
        {
            GameObject.Destroy(lineObjects[i]);
        }

        Vector3 direction = new Vector3(-1, -1, 0).normalized;

        Matrix4x4 translationMatrix = Matrix4x4.Translate(direction * speed * Time.deltaTime);

        translationMatrix1 = translationMatrix;

        foreach (GameObject point in points)
        {
            point.transform.position = translationMatrix.MultiplyPoint(point.transform.position);
        }

        lineRenderers[0] = CreateLine(points[0], points[1]);
        lineRenderers[1] = CreateLine(points[1], points[2]);
        lineRenderers[2] = CreateLine(points[2], points[0]);
    }

    public void ScaleTriangle()
    {
        float.TryParse(ScaleFactor.text, NumberStyles.Float, CultureInfo.InvariantCulture, out float scaleFactor);
        GameObject[] lineObjects = GameObject.FindGameObjectsWithTag("Line");
        for (int i = 0; i < lineObjects.Length; ++i)
        {
            GameObject.Destroy(lineObjects[i]);
        }

        Matrix4x4 scaleMatrix = Matrix4x4.Scale(new Vector3(scaleFactor, scaleFactor, 1f));

        scaleMatrix1 = scaleMatrix;

        foreach (GameObject point in points)
        {
            Vector3 currentPosition = point.transform.position;
            Vector3 scaledPosition = scaleMatrix.MultiplyPoint(currentPosition);
            point.transform.position = scaledPosition;
        }

        lineRenderers[0] = CreateLine(points[0], points[1]);
        lineRenderers[1] = CreateLine(points[1], points[2]);
        lineRenderers[2] = CreateLine(points[2], points[0]);
    }

    public void PrintMatrix()
    {
        Matrix4x4 transformationMatrix = translationMatrix1 * scaleMatrix1;

        string matrixString = MatrixToString(transformationMatrix);

        string savePath = "Assets/TransformMatrix.txt";
        File.WriteAllText(savePath, matrixString);
        Debug.Log("�������-��������� ��������� � ����: " + savePath);
    }
    string MatrixToString(Matrix4x4 matrix)
    {
        string matrixString = "";

        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                matrixString += matrix[i, j] + " ";
            }
            matrixString += "\n";
        }

        return matrixString;
    }

    public string savePath = "Assets/CapturedImage.png";

    public int imageWidth = 1920;
    public int imageHeight = 1080;

    public void SaveInitialImage()
    {
        System.IO.File.WriteAllBytes(savePath, bytes);

        Debug.Log("���������� � MainCamera ���������: " + savePath);
    }
}
